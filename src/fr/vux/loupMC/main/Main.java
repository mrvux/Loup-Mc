package fr.vux.loupMC.main;

import fr.vux.loupMC.commands.LoupMC;
import fr.vux.loupMC.events.Foodsafety;
import fr.vux.loupMC.events.PlayerQuitGame;
import fr.vux.loupMC.events.PlayerTouch;
import fr.vux.loupMC.game.Party;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{

    public static Plugin plugin;


    @Override
    public void onEnable(){
        Main.plugin = this;
        Config.checkConfigs();
        new Party();

        //Commands
        getCommand("loupMC").setExecutor(new LoupMC());

        //Events
        getServer().getPluginManager().registerEvents(new PlayerTouch(), plugin);
        getServer().getPluginManager().registerEvents(new PlayerQuitGame(), plugin);
        getServer().getPluginManager().registerEvents(new Foodsafety(), plugin);


        logInfo("Plugin is enable");
    }

    @Override
    public void onDisable(){
        logInfo("Plugin is disable");
    }



    public void logInfo(String text){
        String logPrefix = "[LoupMC] ";
        Bukkit.getLogger().info(logPrefix + text);
    }

}
