package fr.vux.loupMC.events;

import fr.vux.loupMC.game.Party;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class Foodsafety implements Listener{

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e){
        if(e.getEntity() instanceof Player){
            Player player = (Player) e.getEntity();
            if(Party.currentParty.playerScoreboard.containsKey(player)){
                e.setCancelled(true);
            }
        }
    }

}
