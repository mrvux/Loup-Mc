package fr.vux.loupMC.events;

import fr.vux.loupMC.game.Party;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitGame implements Listener{

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        Party party = Party.currentParty;
        if(party.playerScoreboard.containsKey(e.getPlayer()) && !party.getState().equals("stop")){
            party.playerScoreboard.remove(e.getPlayer());
            party.updateScoreboardNumberPlayers();

            if(party.isWolf(e.getPlayer()) && party.getState().equals("start")){
                party.selectNewWolf();

                for(Player player : party.playerScoreboard.keySet()){
                    player.sendMessage("[LoupMC] " + e.getPlayer().getName() + " vient de quitter la partie");
                    player.sendMessage("[LoupMC] " + party.getWolfName() + " est le nouveau loup");
                }
            }else{
                for(Player player : party.playerScoreboard.keySet()){
                    player.sendMessage("[LoupMC] " + e.getPlayer().getName() + " vient de quitter la partie");
                }
            }


        }
    }

}
