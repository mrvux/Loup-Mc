package fr.vux.loupMC.events;

import fr.vux.loupMC.game.Party;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PlayerTouch implements Listener{

    @EventHandler
    public void onEntityHitEntity(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player && e.getEntity() instanceof Player){
            Player damager = (Player) e.getDamager();
            e.setDamage(0);
            if(Party.currentParty.isWolf(damager)){
                Party.currentParty.setWolf( (Player) e.getEntity() );
            }
        }
    }

}
